@extends('layouts.app')


@section('content')

    <div class="container">


        <h2>Список групп</h2>
        <ol class="breadcrumb">
            <li><a href="{{route('start.index')}}">Главная</a></li>
            <li class="active">Группы</li>
        </ol>

        <hr>


        <table class = "table table-striped">
            <thead>
            <th>Название группы</th>
            <th>Описание</th>
            <th class="text-right">Просмотр</th>
            <th class="text-right">Редактировать</th>
            <th class="text-right">Удалить</th>
            </thead>
            <tbody>
            @forelse ($groups as $group)
                <tr>
                    <td class="text-left"> {{$group->title}}</td>
                    <td>{{$group->description}}</td>

                    <td class="text-right">
                        <a class="btn btn-default" href="{{route('groups.show', $group)}}"><i class="fa
                     fa-th-list"></i></a>
                    </td>

                    <td class="text-right">
                        <a class="btn btn-default" href="{{route('groups.edit', $group)}}"><i class="fa
                     fa-edit"></i></a>
                    </td>

                    <td class="text-right">
                        <form onsubmit="if(confirm('Удалить?')){return true}else{return false}" action="{{route('groups.destroy',$group)}}" method="post">
                            <input type="hidden" name="_method" value="delete">
                            {{csrf_field()}}

                            <button type="submit" class="btn"><i class="fa fa-trash-o"></i></button>
                        </form>
                    </td>
                </tr>
            @empty
            @endempty
            </tbody>
        </table>
        <hr />
        <a href="{{route('groups.create')}}" class="btn btn-primary pull-right">Создать группу</a>
    </div>
@endsection