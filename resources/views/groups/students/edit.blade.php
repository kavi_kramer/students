@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>Редактирование студента</h2>
        <ol class="breadcrumb">
            <li><a href="{{route('groups.index')}}">Студенты</a></li>
            <li class="active">Редактировать</li>
        </ol>

        <hr />

        <form class="form-horizontal" action="{{route('groups.students.update',[$groups, $student])}}" method="post">
            <input type="hidden" name="_method" value="put">
            {{csrf_field()}}

            <label for="">ФИО</label>
            <input type="text" class="form-control" name="name" placeholder={{$student->name}}>
            <label for="">Дата рождения</label>
            <input type="text" class="form-control" name="birthday" placeholder={{$student->birthday}} >
            <hr />
            <input class="btn btn-primary" type="submit" value="Изменить">
        </form>

    </div>
@endsection