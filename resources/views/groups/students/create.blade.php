@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>Добавление студента</h2>
        <ol class="breadcrumb">
            <li class="active">Добавить</li>
        </ol>

        <hr />
        <form class="form-horizontal" action="{{route('groups.students.store',[$groups])}}" method="post">
            <input type="hidden" name="group_id" value="{{$groups}}">
            {{csrf_field()}}
            <div class="form-group"><label class="col-sm-2 control-label">Предметы:</label>
                <div class="col-sm-10">
                    @foreach($subjects as $subject)
                    <input type="hidden" name="subjects[]" value="{{$subject->id}}">
                      @endforeach
                </div>
            </div>


            <label for="">ФИО студента</label>
            <input type="text" class="form-control" name="name" placeholder="Фамилия Имя Отчество">
            <label for="">Дата рождения</label>
            <input type="text" class="form-control" name="birthday"  placeholder="Дата рождения">
            <hr />
            <input class="btn btn-primary" type="submit" value="Добавить">
        </form>


    </div>
@endsection