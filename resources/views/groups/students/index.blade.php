@extends('layouts.app')


@section('content')

    <div class="container">

        <h2>Список студентов</h2>
        <ol class="breadcrumb">
            <li><a href="{{route('groups.index')}}">Группы</a></li>
            <li class="active">Студенты</li>
        </ol>

        <hr>

        <a href="{{route('students.create')}}" class="btn btn-primary pull-right">Добавить студента</a>
        <table class = "table table-striped">
            <thead>
            <th>Название группы</th>
            <th>ФИО студента</th>
            <th>Дата рождения</th>
            <th class="text-right">Редактировать</th>
            <th class="text-right">Удалить</th>
            </thead>
            <tbody>

             @forelse ($students as $student)
                <tr>
                    <td>{{$student->title}}</td>
                    <td>{{$student->name}}</td>
                    <td>{{$student->birthday}}</td>

                    <td class="text-right">
                        <a class="btn btn-default" href="{{route('students.edit', $student)}}"><i class="fa
                     fa-edit"></i></a>
                    </td>

                    <td class="text-right">
                        <form onsubmit="if(confirm('Удалить?')){return true}else{return false}" action="{{route('students.destroy',$student)}}" method="post">
                            <input type="hidden" name="_method" value="delete">
                            {{csrf_field()}}

                            <button type="submit" class="btn"><i class="fa fa-trash-o"></i></button>
                        </form>
                    </td>
                </tr>
            @empty
                @endempty

            </tbody>
        </table>
    </div>
@endsection