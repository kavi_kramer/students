@extends('layouts.app')


@section('content')

    <div class="container">

        <h2>Список студентов</h2>
        <ol class="breadcrumb">
            <li><a href="{{route('groups.index')}}">Группы</a></li>
            <li class="active">Студенты</li>
        </ol>

        <hr>

        <table class = "table table-striped">
            <h1 class="text-center">{{$group->title}}</h1>
            <p class="text-center">{{$group->description}}</p>
            <thead>
            <th>ФИО студента</th>
            <th>Дата рождения</th>
            <th class="text-center">Русский язык</th>
            <th class="text-center">Математика</th>
            <th class="text-center">История</th>
            <th class="text-right">Редактировать</th>
            <th class="text-right">Удалить</th>
            </thead>
            <tbody>
            @forelse($group->students as $student)
                <tr>

                    <td>{{$student->name}}</td>
                    <td>{{$student->birthday}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="text-right">
                        <a class="btn btn-default" href="{{route('groups.students.edit',[$group->id, $student->id])}}"><i class="fa
                     fa-edit"></i></a>
                    </td>
                    <td class="text-right">
                        <form onsubmit="if(confirm('Удалить?')){return true}else{return false}" action="{{route('groups.students.destroy',[$group->id, $student])}}" method="post">
                            <input type="hidden" name="_method" value="delete">
                            {{csrf_field()}}
                            <button type="submit" class="btn"><i class="fa fa-trash-o"></i></button>
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                <td></td>
                    <td></td>
                    <td><h3 class="text-center">Студентов нет</h3></td>
                    <td class="text-right"></td>
                    <td class="text-right"></td>
                </tr>
            @endforelse
            </tbody>
        </table>
        <hr />
         <a href="{{route('groups.students.create', [$group->id])}}" class="btn btn-primary pull-right">Добавить студента</a>
    </div>

@endsection