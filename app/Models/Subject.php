<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $guarded = [];
    protected $primaryKey = 'id';
    public function students()
    {
        return $this->belongsToMany(Student::class);
    }
}
