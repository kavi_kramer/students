<?php

namespace App\Http\Controllers;

use App\Models\Group;
use App\Models\Student;
use App\Models\Subject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($group_id)
    {

    return view ('groups.students.create',[
        'groups'=>$group_id,
        'student'=>[],
        'subjects'=>Subject::get()
    ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
        $request->all(),
        ['name' => 'required',
            'birthday'=>'required'
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withInput();
        }

        $student = Student::create($request->all());
        if($request->input('subjects')):
        $student->subjects()->attach($request->input('subjects'));
        endif;
        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $group_id, $student_id)
    {
        $groups=$group_id;
        $student = Student::find($student_id);
        return view ('groups.students.edit')->with('student',$student)->with('groups',$groups);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Group $group, Student $student)
    {
        $validator = Validator::make(
            $request->all(),
            ['name' => 'required',
                'birthday'=>'required'
            ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withInput();
        }
        $student->update($request->all());
        return redirect()->route('groups.index', $student);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Group $group, Student $student)

    {  
        $student->delete();
        return redirect()->back();
    }
}
